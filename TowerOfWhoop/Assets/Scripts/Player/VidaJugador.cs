using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VidaJugador : MonoBehaviour
{
    [SerializeField] int vida;
    [SerializeField] int maximoVida;

    private void Start()
    {
        vida = maximoVida;
    }

    public void TomarDa�o(int da�o)
    {
        //CinemachineMovimientoCamara.Instance.MoverCamara(5, 5, 2f);
        vida -= da�o;
        if (vida <= 0)
        {
            Destroy(gameObject);
            SceneManager.LoadScene("Respawn");
        }
    }

    void Update()
    {
        if (SceneManager.GetActiveScene().name == "Respawn")
            BGmusic.instance.GetComponent<AudioSource>().Pause();

        if (SceneManager.GetActiveScene().name == "Torre")
            BGmusic.instance.GetComponent<AudioSource>().Play();
    }
}
