using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public float runSpeed;
    Rigidbody myRB;

    bool facingRight;
    bool grounded = false;
    Collider[] groundCollisions;
    float groundCheckRadius = 0.2f;
    public LayerMask groundLayer;
    public Transform groundCheck;
    public float jumpHeight;

    [SerializeField] private AudioClip saltoSonido;

    void Start()
    {
        myRB = GetComponent<Rigidbody>();
        facingRight = true;
    }

    private void Update()
    {

    }

    private void FixedUpdate()
    {
        if (grounded && Input.GetAxis("Jump") > 0)
        {
            grounded = false;
            myRB.AddForce(new Vector3(0, jumpHeight, 0));
           ControladorSonido.Instance.EjecutarSonido(saltoSonido);
        }

        groundCollisions = Physics.OverlapSphere(groundCheck.position, groundCheckRadius, groundLayer);
        if (groundCollisions.Length > 0) grounded = true;
        else grounded = false;

        float move = Input.GetAxis("Horizontal");
        myRB.velocity = new Vector3(move * runSpeed, myRB.velocity.y, 0);

        if (move > 0 && !facingRight) Flip();
        else if (move > 0 && facingRight) Flip();

    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.z *= -1;
        transform.localScale = theScale;
    } 
}
