using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlMenuPrincipal : MonoBehaviour
{

    public void Jugar()
    {
        SceneManager.LoadScene ("Torre");
    }

    public void MenuPrincipal()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Loading()
    {
        SceneManager.LoadScene("LoadingScreen");
    }

    public void Salir()
    {
        Debug.Log("Saliste del juego :)");
        Application.Quit();
    }

}


