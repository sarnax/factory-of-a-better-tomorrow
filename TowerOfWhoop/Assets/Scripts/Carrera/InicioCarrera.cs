using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InicioCarrera : MonoBehaviour
{
    [SerializeField] private Carrera carrera;

    private void OnTriggerEnter (Collider other)
    {
        if (other.CompareTag("Player"))
        {
            carrera.ActivarTemporizador();
            Destroy(gameObject);
        }
    }
}
